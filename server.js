const express = require('express')
const app = express()

let todo = [
    {
        name: 'wat',
        id: 1
    },
    {
        name: 'butsing',
        id: 2
    }
]

app.get('/todo',(req, res) => {
    res.send(todo)
})

app.listen(3000, () => {
    console.log('TODO API STARTED ADD PORT 3000')
})

app.post('/todo', (req, res) => {
    let newTodo = {
        name: 'read a book',
        id: 3
    }

    todo.push(newTodo)
    res.status(201).send()
})
